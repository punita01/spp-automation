#############################################
# Import the modules needed to run the script
#############################################
import json
import logging
from optparse import OptionParser
import copy
import sys
import client
import datetime
import time
import os
import Tkinter
import tkFileDialog
from os.path import isfile,join
logging.basicConfig()
logger = logging.getLogger('logger')
logger.setLevel(logging.INFO)

all_maps =[]
getall_maps = []  

#----------------------
# Validate Credentials
#----------------------

def validate_input():
    if(userinput['username'] is None or userinput['password'] is None or userinput['host'] is None or
       userinput['backupvms'] is None):
        logger.erroe("Please enter credentials")
        sys.exit(2)
        
def prettyprint(indata):
    print json.dumps(indata, sort_keys=True,indent=4, separators=(',', ': '))
        
#----------------------------        
# script to assign sla to vms
#----------------------------

def get_vm_info():
    vmarray = []
    for vm in userinput['vms']:
        vmdata = {}
        searchdata = {"name":vm,"hypervisorType":"vmware"}
        vmsearch = client.SppAPI(session, 'corehv').post(path="/search?resourceType=vm&from=hlo", data=searchdata)['vms']
        if not vmsearch:
            logger.warning("Did not find VM " + vm)
            break
        for foundvm in vmsearch:
            if(foundvm['name'] == vm):
                vmdata['href'] = foundvm['links']['self']['href']
                vmdata['id'] = foundvm['id']
                vmdata['metadataPath'] = foundvm['metadataPath']
                vmarray.append(copy.deepcopy(vmdata))
                logger.info("Adding VM " + vm + " to SLA " + userinput['sla'])
                break
    return vmarray

def get_sla_info():
    slaarray = []
    sladata = {}
    slapols = client.SppAPI(session, 'sppsla').get()['slapolicies']
    for sla in slapols:
        if(sla['name'] == userinput['sla']):
            sladata['href'] = sla['links']['self']['href']
            sladata['id'] = sla['id']
            sladata['name'] = sla['name']
            slaarray.append(copy.deepcopy(sladata))
            break
    if not slaarray:
        logger.error("No SLA Policy found with name " + userinput['sla'])
        session.logout()
        sys.exit(2)
    else:
        return slaarray

def assignsla_tovms():
    
    vcenter_id = userinput['vcenter_id']
    all_vms = client.SppAPI(session, 'api/hypervisor/').get(path= vcenter_id +"/vm?from=hlo")
    instances = all_vms['vms']
    backup_instance = userinput['vms']
    for inst in instances:
        if(inst['name'] == backup_instance):
                backup_href = inst['links']['self']['href']
                backup_id = inst['id']
                backup_metadataPath = inst['metadataPath']
                
    instance_list = backup_instance.split(",")
    instance_info = []
    instdata = {}
    for inst in instance_list:
        for each_inst in instances:
            if(each_inst['name'] == inst):
                instdata['href'] = each_inst['links']['self']['href']
                instdata['id'] = each_inst['id']
                instdata['metadataPath'] = each_inst['metadataPath']
                instance_info.append(copy.deepcopy(instdata))
                logger.info("Adding instance " + inst + " to SLA ")
                break
    all_policies_data = client.SppAPI(session, 'ngp/slapolicy').get(path="?status=true&subtype=vmware")
    all_policies = all_policies_data['slapolicies']
    assign_sla = userinput['sla']
    sla_list = assign_sla.split(",")
    instance_list = backup_instance.split(",")
    slaarray = []
    sladata = {}
    for sla in sla_list:
        for each_sla in all_policies:
            if(each_sla['name'] == sla):
                sladata['href'] = each_sla['links']['self']['href']
                sladata['id'] = each_sla['id']
                sladata['name'] = each_sla['name']
                slaarray.append(copy.deepcopy(sladata))
                break
        if not slaarray:
            logger.error("No SLA Policy found with name " + user_sla)
            session.logout()
            sys.exit(2)
        
    assigndata = {}
    assigndata['subtype'] = "vmware"
    assigndata['version'] = "1.0"
    assigndata['resources'] = instance_info
    assigndata['slapolicies'] = slaarray
    resp = client.SppAPI(session, 'ngp/hypervisor').post(path='?action=applySLAPolicies', data=assigndata)
    logger.info("Vms are now assigned")

#-----------------------------------------
# Script to create a new SLA policy in SPP
#-----------------------------------------

def build_frequency(freq_type, freq_value,start_time):
    frequency = {}
    if("MINUTE" in freq_type.upper()):
        frequency['type'] = "SUBHOURLY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
    elif("HOUR" in freq_type.upper()):
        frequency['type'] = "HOURLY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
    elif("DAY" in freq_type.upper()):
        frequency['type'] = "DAILY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
    elif("WEEK" in freq_type.upper()):
        frequency['type'] = "WEEKLY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
        frequency['dowList'] = build_weekly_dowlist(frequency['activateDate'])
    elif("MONTH" in freq_type.upper()):
        frequency['type'] = "MONTHLY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
        frequency['domList'] = build_monthly_domlist(frequency['activateDate'])
    else:
        logger.error("Invalid frequency type, must be minute, hour, day, week or month")
        session.logout()
        sys.exit(2)
    return frequency

def build_start_date(start_time):
    sdt = datetime.datetime.strptime(start_time, '%m/%d/%Y %H:%M')
    sdt += datetime.timedelta(minutes=2.5)
    sdt -=  datetime.timedelta(minutes=sdt.minute %5, seconds=sdt.second, microseconds=sdt.microsecond)
    start_time =int(sdt.strftime("%S"))*1000
    return start_time

def build_weekly_dowlist(adate):
    dowlist = [False,False,False,False,False,False,False,False]
    adatedt = datetime.datetime.utcfromtimestamp(adate/1000)
    if(adatedt.weekday() == 6):
        dowlist[1] = True
    elif(adatedt.weekday() == 0):
        dowlist[2] = True
    elif(adatedt.weekday() == 1):
        dowlist[3] = True
    elif(adatedt.weekday() == 2):
        dowlist[4] = True
    elif(adatedt.weekday() == 3):
        dowlist[5] = True
    elif(adatedt.weekday() == 4):
        dowlist[6] = True
    elif(adatedt.weekday() == 5):
        dowlist[7] = True
    return dowlist

def build_monthly_domlist(adate):
    domlist = [False] * 32
    adatedt = datetime.datetime.utcfromtimestamp(adate/1000)
    domlist[adatedt.day] = True
    return domlist

def build_sla_policy():
    slainfo = {"name":userinput['sla_name'],
               "version":"1.0",
               "spec":{"simple":True,
                       "subpolicy":[{"type":"REPLICATION",
                                     "software":True,
                                     "retention":{"age":userinput['retention_value']},
                                     "trigger":build_frequency(userinput['frequency_type'], userinput['frequency_value'],userinput['start_time']),
                                     "site":userinput['sla_site']}]}}
    return slainfo

def create_sla_policy(slainfo):
    try:
        response = client.SppAPI(session, 'ngp/slapolicy').post(data=slainfo)
        logger.info("SLA Policy " + slainfo['name'] + " is created")
        
    except client.requests.exceptions.HTTPError as err:
        errmsg = json.loads(err.response.content)
        logger.error(errmsg['response'])
        
#---------------------------
#script to delete sla policy
#---------------------------

def find_slapolicy(slaname):
    response = client.SppAPI(session, 'ngp/slapolicy').get(path='')
    slalist = response['slapolicies']
    for sla in slalist:
        if(sla['name'].upper() == slaname.upper()):
            print "found" + sla['name']
            return sla
    
    logger.error("Sla policy not found.")
    session.logout()
    sys.exit(3)

def delete_slapolicy(slapolicy):
    try:
        response = client.SppAPI(session, 'ngp/slapolicy/').delete(slapolicy['name'])
        logger.info("Sla policy " + slapolicy['name'] + " is deleted")
    except client.requests.exceptions.HTTPError as err:
        #print err.response.content
        session.logout()
        sys.exit(4)
             
#--------------------------
# script to edit sla policy
#---------------------------

def edit_sla_policy():
    slainfo = {"name":userinput['edit_slaname'],
               "version":"1.0",
               "spec":{"simple":True,
                       "subpolicy":[{"type":"REPLICATION",
                                     "software":True,
                                     "retention":{"age":userinput['editretention_value']},
                                     "trigger":build_frequency(userinput['editfrequency_type'],userinput['editfrequency_value'],userinput['editstart_time']),
                                     "site":userinput['editsla_site']}]}}
    return slainfo

def update_sla_policy(slainfo):
    sla_inf = find_slapolicy(slainfo['name'])
    sla_id = sla_inf['id']
    try:
        response = client.SppAPI(session, 'ngp/slapolicy/').put(path=sla_id, data=slainfo)
        logger.info("SLA Policy " + slainfo['name'] + " is edited")
        #trying to print response
        
    except client.requests.exceptions.HTTPError as err:
        errmsg = json.loads(err.response.content)
        logger.error(errmsg['response'])
        session.logout()
        sys.exit(4)

#---------------
# backup vm info
#---------------

def get_vm_backup_info():
    buinfo = {}
    for vm in userinput['backupvms']:
        searchdata = {"name":vm,"hypervisorType":"*"}
        vmsearch = client.SppAPI(session, 'corehv').post(path="/search?resourceType=vm&from=hlo", data=searchdata)['vms']
        if not vmsearch:
            logger.warning("Did not find VM " + vm)
            break
        for foundvm in vmsearch:
            if(foundvm['name'] == vm):
                vmbudata = get_vm_version_info(foundvm)
                buinfo[foundvm['name']] = vmbudata
                logger.info('Backup info for '+ vm + ' is as follows:')
    prettyprint(buinfo)
    return buinfo

def get_vm_version_info(vm):
    vmbuinfo = []
    urlpath = vm['config']['hypervisorKey'] + "/vm/" + vm['id'] + "/version?from=hlo"
    versions = client.SppAPI(session, 'corehv').get(path=urlpath)['versions']
    for version in versions:
        data = {}
        data['slaname'] = version['protectionInfo']['storageProfileName']
        data['jobname'] = version['protectionInfo']['policyName']
        data['butime'] = time.ctime(version['protectionInfo']['protectionTime']/1000)[4:].replace("  "," ")
        vmbuinfo.append(data)
    return vmbuinfo

#--------------------------------------------------------
# Script to restore one or more VMWare VMs by name in SPP
#--------------------------------------------------------   

def build_vm_source(restore_vm):
    source = []
    for vm in restore_vm:
        vminfo = get_vm_restore_info(vm)
        if(vminfo is not None):
            source.append(copy.deepcopy(vminfo))
    return source

def get_vm_restore_info(vm):
    vmdata = {}
    searchdata = {"name":vm,"hypervisorType":"vmware"}
    vmsearch = client.SppAPI(session, 'corehv').post(path="/search?resourceType=vm&from=recovery", data=searchdata)['vms']
    if not vmsearch:
        logger.warning("Did not find recoverable VM " + vm)
        return None
    for foundvm in vmsearch:
        if(foundvm['name'] == vm):
            vmdata['href'] = foundvm['links']['self']['href']
            vmdata['metadata'] = {'name':foundvm['name']}
            vmdata['resourceType'] = "vm"
            vmdata['id'] = foundvm['id']
            vmdata['include'] = True
            vmdata['version'] = {}
            vmdata['version']['href'] = foundvm['links']['latestversion']['href']
            vmdata['version']['metadata'] = {'useLatest':True,'name':"Use Latest"}
            logger.info("Adding VM " + vm + " to restore job")
            return vmdata

def build_subpolicy():
    subpolicy = []
    subpol = {}
    subpol['type'] = "IV"
    subpol['destination'] = {"systemDefined": True}
    subpol['option'] = {}
    subpol['option']['protocolpriority'] = "iSCSI"
    subpol['option']['poweron'] = userinput['power_on_after_recovery']
    subpol['option']['continueonerror'] = userinput['continue_restore_iffails']
    subpol['option']['autocleanup'] = userinput['overwrite_forcecleanup']
    subpol['option']['allowsessoverwrite'] = userinput['overwite_virtual_machine']
    if not (userinput['restore_type'].upper()=='production'.upper()):
        subpol['option']['mode'] = userinput['restore_type']
    else:
        subpol['option']['mode'] = 'recovery'
    subpol['option']['vmscripts'] = False
    subpolicy.append(subpol)
    return subpolicy

def restore_vms(vm):
    restore = {}
    sourceinfo = build_vm_source(vm)
    subpolicy = build_subpolicy()
    restore['subType'] = "vmware"
    restore['spec'] = {}
    restore['spec']['source'] = sourceinfo
    restore['spec']['subpolicy'] = subpolicy
    resp = client.SppAPI(session, 'spphv').post(path='?action=restore', data=restore)
    logger.info("vm is now being restored") 

#--------------------------------------
# Script to run a job on-demand for SPP
#--------------------------------------

def find_job_from_name(job_name):
    try:
        response = client.SppAPI(session, 'endeavour').get(path='job')
        joblist = response['jobs']
        for job in joblist:
            if(job['name'].upper() == job_name.upper()):
                return job
        logger.info("No job with provided name found")
        session.logout()
        sys.exit(2)
    except client.requests.exceptions.HTTPError as err:
       # print err.response.content
        session.logout()
        sys.exit(3)

def run_job(job):
    jobrunpath = "job/" + job['id'] + "?action=start&actionname=start"
    try:
        response = client.SppAPI(session, 'endeavour').post(path=jobrunpath)
        logger.info("Running job " + job['name'])
    except client.requests.exceptions.HTTPError as err:
        #print err.response.content
        session.logout()
        sys.exit(4)

#-----------------------------------       
# Script to restore the alternate VM
#-----------------------------------

def alternatebuild_subpolicy(restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup,target_name,target_resourceType,target_href,mapvirtualnetwork_source,mapvirtualnetwork_recovery,mapvirtualnetwork_test,mapsubnet,mapRRPdatastore_source,mapRRPdatastore_destination,getall_maps):
    #print getall_maps
    subpolicy = []
    subpol = {}
    subpol['type'] = "IV"
    subpol['destination']= {}
    subpol['destination']['target']={'name':target_name ,'resourceType':target_resourceType , 'href':target_href}
    subpol['mapvirtualnetwork'] ={mapvirtualnetwork_source:{'recovery':mapvirtualnetwork_recovery, 'test':mapvirtualnetwork_test}}
    subpol['mapRRPdatastore']={mapRRPdatastore_source : mapRRPdatastore_destination }
    if(all_maps != ''):
        subpol['mapsubnet'] = getall_maps
    else:
        subpol['mapsubnet'] = {"systemDefined": mapsubnet }
   
    
    subpol['option'] = {}
    subpol['option']['protocolpriority'] = "iSCSI"
    subpol['option']['poweron'] = power_on_after_recovery
    subpol['option']['continueonerror'] = continue_restore_iffails
    subpol['option']['autocleanup'] = overwrite_forcecleanup
    subpol['option']['allowsessoverwrite'] = overwite_virtual_machine
    if not (restore_type.upper()=='production'.upper()):
        subpol['option']['mode'] = restore_type
    else:
        subpol['option']['mode'] = 'recovery'
    subpol['option']['vmscripts'] = False
    subpolicy.append(subpol)
    return subpolicy
def alternate_restorethe_vms(restore_vms,restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup,target_name,target_resourceType,target_href,mapvirtualnetwork_source,mapvirtualnetwork_recovery,mapvirtualnetwork_test,mapsubnet,mapRRPdatastore_source,mapRRPdatastore_destination,all_maps):
    restore = {}
    sourceinfo = build_vm_source(restore_vms)
    subpolicy = alternatebuild_subpolicy(restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup,target_name,target_resourceType,target_href,mapvirtualnetwork_source,mapvirtualnetwork_recovery,mapvirtualnetwork_test,mapsubnet,mapRRPdatastore_source,mapRRPdatastore_destination,all_maps)
    restore['subType'] = "vmware"
    restore['spec'] = {}
    restore['spec']['source'] = sourceinfo
    restore['spec']['subpolicy'] = subpolicy
    resp = client.SppAPI(session, 'spphv').post(path='?action=restore', data=restore)
    #logger.info(resp)
    logger.info("VMs are now being restored") 

# Aletrnate restore vms add mapping
def add_mapping():
    
    mapping_type = userinput['mapping_type']
    if (mapping_type=="1"):
        subnet = userinput['subnet']
        dhcp = "true"
        dnslist = ''
        metadata = subnet
        dhcp_map = {subnet:{"dhcp":"true" , "dnslist":'', "metadata":{"ip":subnet}}}
        all_maps.append(dhcp_map)
        
    if (mapping_type == "2"):
        subnet = userinput['subnet']
        dhcp = "false"
        subnet_mask = userinput['static_subnet_mask']
        gateway = userinput['static_gateway']
        dnslist = userinput['static_dnslist']
        static_map = {subnet:{"dhcp":"false", "subnet":subnet, "subnetmask":subnet_mask, "gateway":gateway, "dnslist":dnslist, "metadata":{"ip":subnet}}}
        all_maps.append(static_map)

    return all_maps
    
# Alternate restore vms
def alternatevmrestore(restore_vms):
    if(restore_vms is not None):
        restore_vms = restore_vms.split(",")
        build_vm_source(restore_vms)
    #alternate_options(restore_vms)
        key = userinput['hypervisor_key']
        hypervisor = client.SppAPI(session, 'corehv').get(path="/" + key + "?from=hlo")
        view = userinput['view_type']
        if(view == "host"):
            hosts = client.SppAPI(session, 'corehv').get(path="/"+ key +"/host?from=hlo")
            host_list = hosts['hosts']
            vCenter = userinput['vcenter']
            target_name = vCenter
            target_resourceType = "host"
        elif(view == "cluster"):
            clusters = client.SppAPI(session, 'corehv').get(path="/"+ key +"/cluster?from=hlo")
            cluster_list = clusters['clusters']
            vCenter = userinput['vcenter']
            target_name = vCenter
            target_resourceType = "cluster"
        else:
            logger.error("Incorrect input!!")
            session.logout()
            sys.exit(2)
            
            
        networks = client.SppAPI(session, 'corehv').get(path="/"+ key +"/network?from=hlo")
        network_list = networks['networks']
        mapvirtualnetwork_recovery = ''
        mapvirtualnetwork_test = ''
        recovery = userinput['recovery_network']
        test =  userinput['test_network']
            
        for n in network_list:
            if n['nativeObject']['name'] == recovery:
                 mapvirtualnetwork_recovery = n['links']['self']['href']
                        
        if(mapvirtualnetwork_recovery == ''):
            logger.error("Incorrect input for Network settings!!")
            session.logout()
            sys.exit(2)
                
        for n in network_list:   
            if n['nativeObject']['name'] == test:
                mapvirtualnetwork_test  = n['links']['self']['href']
            
            
        if(mapvirtualnetwork_test == ''):
            logger.error("Incorrect input for Network settings!!")
            session.logout()
            sys.exit(2)
                
        subnet_option = userinput['subnet_option']
        if subnet_option == "1":
            mapsubnet = "true"
            getall_maps = ''
            mapRRPdatastore_source = ''
            mapRRPdatastore_destination = ''
        elif subnet_option == "2":
            mapsubnet = "false"
            getall_maps = ''
            mapRRPdatastore_source = ''
            mapRRPdatastore_destination = ''
        elif subnet_option == "3":
            mapsubnet = ''
            getall_maps = add_mapping()
        else:
            logger.error("Incorrect input for Subnet Options!!")
            session.logout()
            sys.exit(2)
                
        volume = client.SppAPI(session, 'corehv').get(path="/"+ key +"/volume?from=hlo")
        volume_list = volume['volumes']
        mapRRPdatastore_destination = ''
        destination = userinput['volume_network']
            
        for v in volume_list:
            if v['name'] == destination:
                mapRRPdatastore_destination = v['links']['self']['href']
            
        if (mapRRPdatastore_destination == ''):
            logger.error("Incorrect input for Destination datastore!!")
            session.logout()
            sys.exit(2)
                
        restore_type = userinput['alternate_restore_type']
        if(restore_type.upper()!="TEST" and  restore_type.upper()!="PRODUCTION" and restore_type.upper()!="CLONE" ):
            logger.error("Restore type must be one of the following: \nTest\nProduction\nClone")
            session.logout()
            sys.exit(2)
           
        mapsource = client.SppAPI(session, 'corehv').get(path="/"+ key +"/network?from=recovery")
        mapsource_networks =  mapsource['networks']
        for s in mapsource_networks:
            if s['name'] == "172.20.x.x Network":
                mapvirtualnetwork_source = s['links']['latestversion']['href']
                #print mapvirtualnetwork_source
                
        volumesource = client.SppAPI(session, 'corehv').get(path="/"+ key +"/volume?from=recovery")
        volumes_list = volumesource['volumes']
        for v in volumes_list:
            if v['name'] == "BCJ-SPP-BU-VMS":
                mapRRPdatastore_source = v['links']['latestversion']['href']
                   
        #Advanced options(true/false)
        power_on_after_recovery = userinput['alternate_power_on_after_recovery']
        if(power_on_after_recovery.upper()!="TRUE" and power_on_after_recovery.upper()!="FALSE"):
            logger.error("Power on after recovery must be true or false")
            session.logout()
            sys.exit(2)
        overwite_virtual_machine= userinput['alternate_overwite_virtual_machine']
        if(overwite_virtual_machine.upper()!="TRUE" and overwite_virtual_machine.upper()!="FALSE"):
            logger.error("Overwrite Virtual machine must be true or false")
            session.logout()
            sys.exit(2)
        continue_restore_iffails= userinput['alternate_continue_restore_iffails']
        if(continue_restore_iffails.upper()!="TRUE" and continue_restore_iffails.upper()!="FALSE"):
            logger.error("Continue with restore even if it fails must be true or false")
            session.logout()
            sys.exit(2)
        overwrite_forcecleanup= userinput['alternate_overwrite_forcecleanup']
        if(overwrite_forcecleanup.upper()!="TRUE" and overwrite_forcecleanup.upper()!="FALSE"):
            logger.error("Allow to overwrite and force clean up of pending old session must be true or false")
            session.logout()
            sys.exit(2)
        target_href = network_list[0]['links']['host']['href']
        
    alternate_restorethe_vms(restore_vms,restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup,target_name,target_resourceType,target_href,mapvirtualnetwork_source,mapvirtualnetwork_recovery,mapvirtualnetwork_test,mapsubnet,mapRRPdatastore_source,mapRRPdatastore_destination,getall_maps)
          
    del all_maps[:]
    
#--------------
# Oracle Backup
#--------------

def oracle_backup():
    
    all_databases = client.SppAPI(session, 'api/application/oracle').get(path="/instance?from=hlo")
    instances = all_databases['instances']
    #print instances
    backup_instance = userinput['oracle_backup_instance']
    for inst in instances:
        if(inst['name'] == backup_instance):
                backup_href = inst['links']['self']['href']
                backup_id = inst['id']
                backup_metadataPath = inst['metadataPath']
                
    oracle_options = userinput['oracle_options']
    if(oracle_options == '1'):
        max_streams = userinput['oracle_max_streams']
        applyOptions = {"resources":[{"href": backup_href,"id":backup_id,"metadataPath":backup_metadataPath}],"options":{"maxParallelStreams":max_streams}}
        backup_opt = client.SppAPI(session, 'ngp/application').post(path='?action=applyOptions', data=applyOptions)
        print ("Options updated for the Oracle instance!")
    elif(oracle_options == '2'):
        all_policies_data = client.SppAPI(session, 'ngp/slapolicy').get(path="?status=true&subtype=oracle")
        all_policies = all_policies_data['slapolicies']
        assign_sla = userinput['oracle_assign_sla']
        instance_list = backup_instance.split(",")
        instance_info = []
        instdata = {}
        for inst in instance_list:
            for each_inst in instances:
                if(each_inst['name'] == inst):
                    instdata['href'] = each_inst['links']['self']['href']
                    instdata['id'] = each_inst['id']
                    instdata['metadataPath'] = each_inst['metadataPath']
                    instance_info.append(copy.deepcopy(instdata))
                    logger.info("Adding instance " + inst + " to SLA ")
                    break

        slaarray = []
        sla_list = assign_sla.split(",")
        sladata = {}
        for sla in sla_list:
            for each_sla in all_policies:
                if(each_sla['name'] == sla):
                    sladata['href'] = each_sla['links']['self']['href']
                    sladata['id'] = each_sla['id']
                    sladata['name'] = each_sla['name']
                    slaarray.append(copy.deepcopy(sladata))
                    break
            if not slaarray:
                logger.error("No SLA Policy found with name " + user_sla)
                session.logout()
                sys.exit(2)
        
        assigndata = {}
        assigndata['subtype'] = "oracle"
        assigndata['version'] = "1.0"
        assigndata['resources'] = instance_info
        assigndata['slapolicies'] = slaarray
        resp = client.SppAPI(session, 'ngp/application').post(path='?action=applySLAPolicies', data=assigndata)
        logger.info("Oracle instances are now assigned")

#---------------        
# Oracle Restore
#---------------

def restore_Orc(continue_on_error,database_href,database_version,database_name,database_id,sub_type,autocleanup,allowsessoverwrite,continueonerror,protocol_priority,overwriteExistingDb,maxParallelStreams,initParams):
        restore = {"subType":"oracle",
     "script":{"preGuest":None,"postGuest":None,"continueScriptsOnError":continue_on_error},
     "spec":
     {"source":[{"href":database_href,"resourceType":"database","include":True,
                 "version":{"href":database_version,
                            "metadata":{"useLatest":True}},
                 "metadata":{"name":database_name},"id":database_id}],
      "subpolicy":[{"type":"restore","mode":sub_type,
                    "destination":{"mapdatabase":{database_href:{"name":"","paths":[]}}},
                    "option":{"autocleanup":autocleanup,"allowsessoverwrite":allowsessoverwrite,"continueonerror":continueonerror,"protocolpriority":protocol_priority,
                              "applicationOption":{"overwriteExistingDb":overwriteExistingDb,"maxParallelStreams":maxParallelStreams,"initParams":initParams}},
                    "source":{"copy":{"site":{"href":userinput['host'] + ":443/api/site/1000"}}}}],
      "view":"applicationview"}}
        resp = client.SppAPI(session, 'ngp/application').post(path='?action=restore', data=restore)
        logger.info("Oracle database is restored")
        
def restore_orc_alternate(continue_on_error,database_href,database_version,database_name,database_id,sub_type,traget_href,autocleanup,allowsessoverwrite,continueonerror,protocol_priority):
            restore = {"subType":"",
             "script":{"preGuest":None,"postGuest":None,"continueScriptsOnError":continue_on_error},
             "spec":{"source":[{"href":database_href,"resourceType":"database","include":True,
                                "version":{"href":database_version,
                                           "metadata":{"useLatest":True}},
                                "metadata":{"name":database_name},"id":database_id}],
                     "subpolicy":[{"type":"IA","mode":sub_type,
                                   "destination":
                                   {"target":{"href":traget_href,"resourceType":"applicationinstance"}},
                                   "option":{"autocleanup":autocleanup,"allowsessoverwrite":allowsessoverwrite,"continueonerror":continueonerror,"protocolpriority":protocol_priority},
                                   "source":{"copy":{"site":{"href":userinput['host'] + ":443/api/site/1000"}}}}],
                     "view":"applicationview"}}
            resp = client.SppAPI(session, 'ngp/application').post(path='?action=restore', data=restore)
            logger.info("Oracle database restored with alternate instance")

def oracle_restore():
    allrecovery_instances = client.SppAPI(session, 'api/application/oracle').get(path="/instance?from=recovery")
    recovery_instances = allrecovery_instances['instances']
    instance_torecover = userinput['oracle_instance_torecover']
    for inst in recovery_instances:
        if inst['name'] == instance_torecover:
            inst_id = inst['id']
            break
    all_databases = client.SppAPI(session, 'api/application/oracle').get(path="/instance/" +inst_id+ "/database?from=recovery")
    databases = all_databases['databases']
    
    database_torestore = userinput['oracle_database_torestore']
    for data in databases:
        if(data['name'] == database_torestore):
            database_id = data['id']
            database_href = data['links']['self']['href']
            database_version = data['links']['latestversion']['href']
            database_resourcetype = data['resourceType']
            database_name = data['name']
            break
       
    continue_on_error = userinput['oracle_continue_on_error']
    autocleanup = userinput['oracle_autocleanup']
    allowsessoverwrite = userinput['oracle_allowsessoverwrite']
    continueonerror = userinput['oracle_continueonerror']
    overwriteExistingDb = userinput['oracle_overwriteExistingDb']
    maxParallelStreams = userinput['oracle_maxParallelStreams']
    
    sub_type = userinput['oracle_subpolicy_type']
    if sub_type.upper() == "TEST":
        sub_type = "test"
        protocol_priority = None
        initParams = userinput['oracle_initial_parameters']
    elif sub_type.upper() == "PRODUCTION":
        sub_type = "production"
        protocol_priority = None
        initParams = userinput['oracle_initial_parameters']
    elif sub_type.upper() == "INSTANT_ACCESS":
        sub_type = "test"
        protocol_priority = userinput['oracle_protocol_priority']
        initParams = None   
    
    destination = userinput['oracle_destination']
    if destination == "1":
        restore_Orc(continue_on_error,database_href,database_version,database_name,database_id,sub_type,autocleanup,allowsessoverwrite,continueonerror,overwriteExistingDb,maxParallelStreams,protocol_priority,initParams)
    elif destination == '2':
        
        all_databases = client.SppAPI(session, 'api/application/oracle').get(path="/instance?from=hlo")
        instances = all_databases['instances']
        
        target_instance = userinput['oracle_target_instance']
        for inst in instances:
            if inst['name'] == target_instance:
                traget_href = inst['links']['self']['href']
        restore_orc_alternate(continue_on_error,database_href,database_version,database_name,database_id,sub_type,traget_href,autocleanup,allowsessoverwrite,continueonerror,protocol_priority)

#-----------        
# SQL Backup
#-----------

def sql_backup():
    all_databases = client.SppAPI(session, 'api/application/sql').get(path="/instance?from=hlo")
    instances = all_databases['instances']
    
    backup_instance = userinput['sql_backup_instance']
    for inst in instances:
        if(inst['name'] == backup_instance):
                backup_href = inst['links']['self']['href']
                backup_id = inst['id']
                backup_metadataPath = inst['metadataPath']
                
    sql_options = userinput['sql_options']
    if(sql_options == '1'):
        max_streams = userinput['sql_max_streams']
        log_backup = userinput['sql_log_backup']
        if(log_backup == "false"):
            applyOptions = {"resources":[{"href": backup_href,"id":backup_id,"metadataPath":backup_metadataPath}],"options":{"maxParallelStreams":max_streams,"logbackup":{}}}
        else: 
            frequency = userinput['sql_frequency']
            frequency_type = userinput['sql_frequency_type']
            trigger_time = userinput['sql_trigger_time']
            
            applyOptions = {"resources":[{"href": backup_href,"id":backup_id,"metadataPath":backup_metadataPath}],"options":{"maxParallelStreams":max_streams,"logbackup":{"performlogbackup":True,"rpo":{"frequency":frequency,"frequencyType":frequency_type.upper(),"triggerTime":trigger_time,"metadata":{"activateDate":1521518400000}}}}}
        
        backup_opt = client.SppAPI(session, 'ngp/application').post(path='?action=applyOptions', data=applyOptions)
        print ("Options updated for the sql instance!")
        
    elif(sql_options == '2'):
        all_policies_data = client.SppAPI(session, 'ngp/slapolicy').get(path="?status=true&subtype=oracle")
        all_policies = all_policies_data['slapolicies']
        
        assign_sla = userinput['sql_assign_sla']
        instance_list = backup_instance.split(",")
        instance_info = []
        instdata = {}
        for inst in instance_list:
            for each_inst in instances:
                if(each_inst['name'] == inst):
                    instdata['href'] = each_inst['links']['self']['href']
                    instdata['id'] = each_inst['id']
                    instdata['metadataPath'] = each_inst['metadataPath']
                    instance_info.append(copy.deepcopy(instdata))
                    logger.info("Adding instance " + inst + " to SLA ")
                    break

        slaarray = []
        sla_list = assign_sla.split(",")
        sladata = {}
        for sla in sla_list:
            for each_sla in all_policies:
                if(each_sla['name'] == sla):
                    sladata['href'] = each_sla['links']['self']['href']
                    sladata['id'] = each_sla['id']
                    sladata['name'] = each_sla['name']
                    slaarray.append(copy.deepcopy(sladata))
                    break
            if not slaarray:
                logger.error("No SLA Policy found with name " + user_sla)
                session.logout()
                sys.exit(2)
        
        assigndata = {}
        assigndata['subtype'] = "sql"
        assigndata['version'] = "1.0"
        assigndata['resources'] = instance_info
        assigndata['slapolicies'] = slaarray
        resp = client.SppAPI(session, 'ngp/application').post(path='?action=applySLAPolicies', data=assigndata)
        logger.info("sql instnaces are now assigned")

#-----------        
#Restore sql
#-----------

def restore_sql(continue_on_error,database_href,database_version,database_name,database_id,sub_type,autocleanup,allowsessoverwrite,continueonerror,overwriteExistingDb,maxParallelStreams,recoverymode):
        restore = {"subType":"sql",
                   "script":{"preGuest":None,"postGuest":None,"continueScriptsOnError":continue_on_error},
                   "spec":{"source":[{"href":database_href,"resourceType":"database","include":True,
                                      "version":{"href":database_version,
                                                 "metadata":{"useLatest":True}},"metadata":{"name":database_name},"id":database_id}],
                           "subpolicy":[{"type":"restore","mode":sub_type,"destination":
                                         {"mapdatabase":{database_href:{"name":"","paths":[]}}},
                                         "option":{"autocleanup":autocleanup,"allowsessoverwrite":allowsessoverwrite,"continueonerror":continueonerror,"applicationOption":{"overwriteExistingDb":overwriteExistingDb,"maxParallelStreams":maxParallelStreams,"recoverymode":recoverymode}},
                                         "source":{"copy":{"site":{"href":userinput['host'] + ":443/api/site/1000"}}}}],
                           "view":"applicationview"}}
        resp = client.SppAPI(session, 'ngp/application').post(path='?action=restore', data=restore)
        logger.info("Sql database is restored")
        
def restore_sql_alternate(continue_on_error,database_href,database_version,database_name,database_id,sub_type,traget_href,autocleanup,allowsessoverwrite,continueonerror,protocol_priority):
            restore = {"subType":"",
             "script":{"preGuest":None,"postGuest":None,"continueScriptsOnError":continue_on_error},
             "spec":{"source":[{"href":database_href,"resourceType":"database","include":True,
                                "version":{"href":database_version,
                                           "metadata":{"useLatest":True}},
                                "metadata":{"name":database_name},"id":database_id}],
                     "subpolicy":[{"type":"IA","mode":sub_type,
                                   "destination":
                                   {"target":{"href":traget_href,"resourceType":"applicationinstance"}},
                                   "option":{"autocleanup":autocleanup,"allowsessoverwrite":allowsessoverwrite,"continueonerror":continueonerror,"protocolpriority":protocol_priority},
                                   "source":{"copy":{"site":{"href":userinput['host'] + ":443/api/site/1000"}}}}],
                     "view":"applicationview"}}
            resp = client.SppAPI(session, 'ngp/application').post(path='?action=restore', data=restore)
            logger.info("SQL database restored with alternate instance")
            
def sql_restore():
    allrecovery_instances = client.SppAPI(session, 'api/application/sql').get(path="/instance?from=recovery")
    recovery_instances = allrecovery_instances['instances']
    instance_torecover = userinput['sql_instance_torecover']
    for inst in recovery_instances:
        if inst['name'] == instance_torecover:
            inst_id = inst['id']
            break
    all_databases = client.SppAPI(session, 'api/application/sql').get(path="/instance/" +inst_id+ "/database?from=recovery")
    databases = all_databases['databases']
    database_torestore = userinput['sql_database_torestore']
    for data in databases:
        if(data['name'] == database_torestore):
            database_id = data['id']
            database_href = data['links']['self']['href']
            database_version = data['links']['latestversion']['href']
            database_resourcetype = data['resourceType']
            database_name = data['name']
            break
       
    continue_on_error = userinput['sql_continue_on_error']
    autocleanup = userinput['sql_autocleanup']
    allowsessoverwrite = userinput['sql_allowsessoverwrite']
    continueonerror = userinput['sql_continueonerror']
    overwriteExistingDb = userinput['sql_overwriteExistingDb']
    maxParallelStreams = userinput['sql_maxParallelStreams']
    
    sub_type = userinput['sql_subpolicy_type']
    if sub_type.upper() == "TEST":
        sub_type = "test"
        protocol_priority = None
        recoverymode = userinput['sql_recovery_mode']
    elif sub_type.upper() == "PRODUCTION":
        sub_type = "production"
        protocol_priority = None
        recoverymode = userinput['sql_recovery_mode']
    elif sub_type.upper() == "INSTANT_ACCESS":
        sub_type = "test"
        protocol_priority = userinput['sql_protocol_priority']
        recoverymode = None
    destination = userinput['sql_destination']
    if destination == "1":
        restore_sql(continue_on_error,database_href,database_version,database_name,database_id,sub_type,autocleanup,allowsessoverwrite,continueonerror,overwriteExistingDb,maxParallelStreams,recoverymode)
    elif destination == '2':
        all_databases = client.SppAPI(session, 'api/application/sql').get(path="/instance?from=hlo")
        instances = all_databases['instances']
        target_instance = userinput['sql_target_instance']
        for inst in instances:
            if inst['name'] == target_instance:
                target_href = inst['links']['self']['href']
         
        if(target_href is None):
            logger.error("\nTarget Instnace not found ")
            session.logout()
            sys.exit(2)
                
        restore_sql_alternate(continue_on_error,database_href,database_version,database_name,database_id,sub_type,target_href,autocleanup,allowsessoverwrite,continueonerror,protocol_priority)

#---------------
#Catalog Backup
#---------------
def catalog_backup():
        all_slas = client.SppAPI(session, 'ngp').get(path="/slapolicy?status=true&subtype=catalog")
        policies = all_slas['slapolicies']
        sla_list = userinput['catalog_sla'].split(',')
        slaarray = []
        for sla in sla_list:
            sladata = {}
            for slas in policies:
                if(slas['name'] == sla):
                    sladata['href'] = slas['links']['self']['href']
                    sladata['id'] = slas['id']
                    sladata['name'] = slas['name']
                    slaarray.append(copy.deepcopy(sladata))
                    logger.info("Adding sla " + slas['name'] + " to Catalog ")
                    break
        applysla = {"subtype":"catalog","version":"1.0","slapolicies":slaarray}
        resp = client.SppAPI(session, 'ngp/catalog/system').post(path='?action=applySLAPolicies', data=applysla)
        logger.info("Added sla to Catalog!!")
        
#------------------
#Catalog Retention
#------------------
def catalog_retention():
    jobname = userinput['retention_jobname']
    all_jobs = client.SppAPI(session,'api').get(path="/endeavour/jobsession?sort=%5B%7B%22property%22:%22start%22,%22direction%22:%22DESC%22%7D%5D&filter=%5B%7B%22property%22:%22hasCatalog%22,%22value%22:%22true%22,%22op%22:%22=%22%7D,%7B%22property%22:%22serviceId%22,%22value%22:%5B%22serviceprovider.protection.hypervisor%22,%22serviceprovider.protection.application%22,%22serviceprovider.protection.catalog%22%5D,%22op%22:%22IN%22%7D%5D")
    sessions = all_jobs['sessions']
    #print sessions
    start_date = datetime.datetime.strptime(userinput['retention_start_date'], "%Y-%m-%d").date()
    end_date = datetime.datetime.strptime(userinput['retention_end_date'],"%Y-%m-%d").date()
    for job in sessions:
        if (job['jobName'] == jobname):
            dt_obj_start = datetime.date.fromtimestamp(int(job['start'])/1000)
            dt_obj_end = datetime.date.fromtimestamp(int(job['end'])/1000)
            if (start_date >= dt_obj_start and  start_date <= dt_obj_end) and (end_date >= dt_obj_start and end_date >= dt_obj_end):
                print dt_obj_start
                print dt_obj_end
                resp = client.SppAPI(session, 'api/endeavour/jobsession/'+job['id']).post(path='?action=expire', data={})
                
    
    logger.info(jobname + " in given date range expired!!")
               
        
########################
# Get filepath from user
#######################
Tkinter.Tk().withdraw() # Close the root window
in_path = tkFileDialog.askopenfilename()
filePath = in_path
currentFilePath = os.path.join(filePath)

#-----------------------------------------------
# Using Dictonary to store user input from file
#-----------------------------------------------

try:
    f = open(currentFilePath)
    userinput = {}
    for line in f:
        li= line.lstrip()
        if not li.startswith("#") and '=' in li:
            k, v = line.strip().split('=')
            userinput[k.strip()] = v.strip()
    f.close()
except:
    logger.error('File not found.')
    sys.exit(2)
    
validate_input()  
session = client.SppSession(userinput['host'],userinput['username'],userinput['password'])
session.login()
print "User is logged in!!"

#------------------------
# Checking initial values
#------------------------

if (userinput['sla'] and userinput['vms']):
    if(userinput['vms'] is not None):
        assignsla_tovms()

if(userinput['sla_name'] and userinput['sla_site'] and userinput['retention_type'] and userinput['retention_value'] and 
    userinput['frequency_type'] and userinput['frequency_value'] and userinput['start_time']):
    slainfo = build_sla_policy()
    create_sla_policy(slainfo)

if(userinput['delete_slaname']):
    slapolicy = find_slapolicy(userinput['delete_slaname'])
    delete_slapolicy(slapolicy)
    
if(userinput['edit_slaname']):
    slainfo = edit_sla_policy()
    update_sla_policy(slainfo)
 
if(userinput['restore_vms']):
    if(userinput['restore_vms'] is not None):
        userinput['restore_vms'] = userinput['restore_vms'].split(",")
        restore_vms(userinput['restore_vms'])

if(userinput['job_name']):
    job = find_job_from_name(userinput['job_name'])
    run_job(job)

if(userinput['backupvms']):
    if(userinput['backupvms'] is not None):
        userinput['backupvms'] = userinput['backupvms'].split(",")
        get_vm_backup_info()
        
if(userinput['alternaterestore_vms']):
    alternatevmrestore(userinput['alternaterestore_vms'])
    
if(userinput['oracle_backup_instance']):
    oracle_backup()
    
if(userinput['oracle_instance_torecover']):
    oracle_restore()
    
if(userinput['sql_backup_instance']):
    sql_backup()
    
if(userinput['sql_instance_torecover']):
    sql_restore()
    
if(userinput['catalog_sla']):
    catalog_backup()

if(userinput['retention_jobname']):
    catalog_retention()
       
session.logout()
