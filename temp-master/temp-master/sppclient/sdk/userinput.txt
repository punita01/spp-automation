#user credentials

username=admin
password=sp3ctrum
host=https://172.20.66.120
------------------------------------------------------------
# vms you want to back up

backupvms= 
-------------------------------------------------------------
#create sla policy 
#Iron1
sla_name = 						
# sla site(options): Primary, Replication
sla_site = Primary
# Retention type(options): Days or Snapshots
retention_type = days
retention_value = 10 
# Frequency type(options): minute, hour, day, week or month
frequency_type = hour
frequency_value = 1 
# Start time rounded to nearest 5 minutes, format(mm/dd/yyyy : 02:12)
start_time = 3/27/2018  05:18
-------------------------------------------------------------------------------
#Delete sla policy

delete_slaname = 
--------------------------------------------------------------------------------
#Edit sla policy

edit_slaname = 
# sla site(options): Primary, Replication
editsla_site = Primary		
# Retention type(options): Days or Snapshots
editretention_type = days	
editretention_value = 2
# Frequency type(options): minute, hour, day, week or month
editfrequency_type = hour
editfrequency_value = 2 
# Start time rounded to nearest 5 minutes, format(mm/dd/yyyy : 02:12)
editstart_time = 3/27/2018  05:41
---------------------------------------------------------------------------------
# VM Backup(Assign sla to vms)
#AD,AD_1
vms = 
vcenter_id = 1001
sla = Silver
----------------------------------------------------------------------
#Vm restore
#BCJVM1
restore_vms = 
#Restore type(test/production/clone)
restore_type = production
#Advanced options(true/false)
power_on_after_recovery = true
overwite_virtual_machine= true
continue_restore_iffails= true
overwrite_forcecleanup= true
----------------------------------------------------------------------
#Run Job
#sql_Gold
job_name = 

-------------------------------------------------------------------------------
#Vm alternate restore
#BCJVM1
alternaterestore_vms = 
#view type (host/cluster)
view_type = host
# Host/cluster name to use
hypervisor_key = 1001
vcenter = psdemo4.devad.catalogic.us
recovery_network = 10NET
test_network = 10NET
#subnet option 1.Use system defined subnets and IP addresses for VM guest OS on destination 2.Use original subnets and IP addresses for VM guest OS on destination 3.Add mappings for subnets and IP addresses for VM guest OS on destination (Enter option number)
subnet_option = 3
# If you selected option 3 fill in the mapping options
# mapping type 1.DHCP 2.Static enter option number
mapping_type = 2
subnet = 192.168.2.15

# If you selected option fill static options
static_subnet_mask =192.168.2.16
static_gateway =192.168.2.30
static_dnslist =


volume_network = BRS-Griffin
#Restore type(test/production/clone)
alternate_restore_type = test
#Advanced options(true/false)
alternate_power_on_after_recovery = true
alternate_overwite_virtual_machine= true
alternate_continue_restore_iffails= true
alternate_overwrite_forcecleanup= true

------------------------------------------------------------------------------------------------------------------------------------------
#Oracle Backup(Assign SLA policy)
#pl-oracle-asm / OraDB12Home1
oracle_backup_instance = 
# Perform 1.Maximum Parallel streams 2.Assign slaPolicy(Select Option)
oracle_options = 2
#If option 1
oracle_max_streams = 2

#If option 2
oracle_assign_sla = Gold,Silver
------------------------------------------------------------------------------------
# Oracle Restore
#pl-oracle-asm / OraDB12Home1
oracle_instance_torecover = 
oracle_database_torestore = iDB

oracle_continue_on_error = true
oracle_autocleanup= true
oracle_allowsessoverwrite= true
oracle_continueonerror= true
oracle_overwriteExistingDb= true
oracle_maxParallelStreams= 4

#Subpolicy type (Test,Production,Instant_Access):
oracle_subpolicy_type= test


# Destination 1.Restore to original instance 2.Restore to alternate instance(Select Option)
oracle_destination = 2
oracle_initial_parameters = Source
oracle_protocol_priority = iSCSI
# For option 2
oracle_target_instance = pl-oracle-asm / OraDb11g_home1

---------------------------------------------------------------------------------------------------
# SQL Backup(Assign SLA policy)
# DPXDEMOSQL\DPXDEMOSQL
sql_backup_instance = 
# Perform 1.Maximum Parallel streams 2.Assign slaPolicy(Select Option)
sql_options = 2
#If option 1
sql_max_streams = 2
# sql log backup true/false
sql_log_backup = true
#If logbackup true
sql_frequency = 4
#Frequency type(Minute/Hour,Day,Week,Month):
sql_frequency_type = Hour
# trigger time(HH:MM:SS AM)
sql_trigger_time = 07:01:30 AM


#If option 2
sql_assign_sla = Gold,Silver

--------------------------------------------------------------------------------------
# SQL Restore
# DPXDEMOSQL\DPXDEMOSQL
sql_instance_torecover =  
sql_database_torestore =  DEMOSQLDB1

sql_continue_on_error = true
sql_autocleanup= true
sql_allowsessoverwrite= true
sql_continueonerror= true
sql_overwriteExistingDb= true
sql_maxParallelStreams= 4

# Recovery mode recovery/norecovery
sql_recovery_mode = recovery

#Subpolicy type (Test,Production,Instant_Access):
sql_subpolicy_type= test

# Destination 1.Restore to original instance 2.Restore to alternate instance(Select Option)
sql_destination = 2
sql_protocol_priority = iSCSI
# For option 2
sql_target_instance = RKK-SSSQLNETAPP

------------------------

# Calatlog backup
#Iron,Test1
catalog_sla = 

------------------------

--------------------
# Catalog Retention
# oracle_Gold
retention_jobname = oracle_Gold 
# Date format yyyy-mm-day
retention_start_date = 2018-04-13
retention_end_date = 2018-04-15
--------------------
