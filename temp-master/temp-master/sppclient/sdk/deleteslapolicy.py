#Script to delete slaPolicy

import pdb
import json
import logging
from optparse import OptionParser
import copy
import sys
import client
logging.basicConfig()
logger = logging.getLogger('logger')
logger.setLevel(logging.INFO)

parser = OptionParser()
parser.add_option("--user", dest="username", help="SPP Username")
parser.add_option("--pass", dest="password", help="SPP Password")
parser.add_option("--host", dest="host", help="SPP Host, (ex. https://172.20.49.49)")
parser.add_option("--slapolicyname", dest="slapolicyname", help="sla policy name to delete")
(options, args) = parser.parse_args()

def prettyprint(indata):
    print json.dumps(indata, sort_keys=True,indent=4, separators=(',', ': '))

def validate_input():
    if(options.username is None or options.password is None or options.host is None or
       options.slapolicyname is None):
        print "Invalid input, use -h switch for help"
        sys.exit(1)
        
def find_slapolicy():
    try:
        response = client.SppAPI(session, 'sppsla').get(path='')
        slalist = response['slapolicies']
        for sla in slalist:
            if(sla['name'].upper() == options.slapolicyname.upper()):
                print "The sla to be deleted has id :" + sla['id']
                return sla
                
        print "No sla with provided name found."
        session.logout()
        sys.exit(2)
    except client.requests.exceptions.HTTPError as err:
        print err.response.content
        session.logout()
        sys.exit(3)

def delete_slapolicy(slapolicy):
    deleteslapath = slapolicy['name'] + "?action=start&actionname=start"
    try:
        response = client.SppAPI(session, 'sppsla').delete(deleteslapath)
        print "Sla policy " + slapolicy['name'] + " is deleted"
    except client.requests.exceptions.HTTPError as err:
        print err.response.content
        session.logout()
        sys.exit(4)
        

validate_input()
session = client.SppSession(options.host, options.username, options.password)
session.login()
slapolicy = find_slapolicy()
delete_slapolicy(slapolicy)
session.logout()