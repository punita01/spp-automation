#############################################
# Import the modules needed to run the script
#############################################
import json
import logging
from optparse import OptionParser
import copy
import sys
import client
import datetime
import time
import os
import getpass
import base64
from ConfigParser import SafeConfigParser 
import pdb

from os.path import isfile,join
logging.basicConfig()
logger = logging.getLogger('logger')
logger.setLevel(logging.INFO)
 

menu_actions  = {} 
all_maps =[]
getall_maps = []

##################
# MENUS FUNCTIONS
##################
 
#----------------------
# Validate Credentials
#----------------------

def validate_input(username,password,host):
    if(username is None or password is None or host is None):
        logger.erroe("Please enter credentials")
        sys.exit(2)

def prettyprint(indata):
    print json.dumps(indata, sort_keys=True,indent=4, separators=(',', ': '))

#---------------
# backup vm info
#---------------

def get_vm_backup_info(backupvms):
    buinfo = {}
    for vm in backupvms:
        searchdata = {"name":vm,"hypervisorType":"*"}
        vmsearch = client.SppAPI(session, 'corehv').post(path="/search?resourceType=vm&from=hlo", data=searchdata)['vms']
        if not vmsearch:
            logger.warning("Did not find VM " + vm)
            break
        for foundvm in vmsearch:
            if(foundvm['name'] == vm):
                vmbudata = get_vm_version_info(foundvm)
                buinfo[foundvm['name']] = vmbudata
                logger.info('Backup info for '+ vm + ' is as follows:')
    prettyprint(buinfo)
    return buinfo

def get_vm_version_info(vm):
    vmbuinfo = []
    urlpath = vm['config']['hypervisorKey'] + "/vm/" + vm['id'] + "/version?from=hlo"
    versions = client.SppAPI(session, 'corehv').get(path=urlpath)['versions']
    for version in versions:
        data = {}
        data['slaname'] = version['protectionInfo']['storageProfileName']
        data['jobname'] = version['protectionInfo']['policyName']
        data['butime'] = time.ctime(version['protectionInfo']['protectionTime']/1000)[4:].replace("  "," ")
        vmbuinfo.append(data)
    return vmbuinfo

#-----------------------------------------
# Script to create a new SLA policy in SPP
#-----------------------------------------

def build_sla_policy(sla_name,sla_site,retention_type,retention_value,frequency_type,frequency_value,start_time):
    
    slainfo = {"name":sla_name,
               "version":"1.0",
               "spec":{"simple":True,
                       "subpolicy":[{"type":"REPLICATION",
                                     "software":True,
                                     "retention":{"age":retention_value},
                                     "trigger":build_frequency(frequency_type, frequency_value,start_time),
                                     "site":sla_site}]}}
    return slainfo

def build_site(sla_site):
    sites = client.SppAPI(session, 'coresite').get()['sites']
    for site in sites:
        if(site['name'].upper() == sla_site.upper()):
            return site['name']
    logger.error("Site name not found")
    session.logout()
    sys.exit(2)

def build_retention(retention_type, retention_value):
    retention = {}
    try:
        retention_value = int(retention_value)
    except ValueError:
        logger.error("Invalid retention value, must be integer value")
        session.logout()
        sys.exit(2)
        
    if(retention_type.upper() == "DAYS"):
        retention = {'age': int(retention_value)}
        return retention
    elif(retention_type.upper() == "SNAPSHOTS"):
        retention = {'numsnapshots': int(retention_value)}
        return retention
    
    logger.error("Invalid retention type, must be days or snapshots")
    session.logout()
    sys.exit(2)
    

def build_frequency(freq_type, freq_value,start_time):
    frequency = {}
    try:
        freq_value = int(freq_value)
    except ValueError:
        logger.error("Invalid frequency value, must be integer value")
        session.logout()
        sys.exit(2)
    if("MINUTE" in freq_type.upper()):
        frequency['type'] = "SUBHOURLY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
    elif("HOUR" in freq_type.upper()):
        frequency['type'] = "HOURLY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
    elif("DAY" in freq_type.upper()):
        frequency['type'] = "DAILY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
    elif("WEEK" in freq_type.upper()):
        frequency['type'] = "WEEKLY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
        frequency['dowList'] = build_weekly_dowlist(frequency['activateDate'])
    elif("MONTH" in freq_type.upper()):
        frequency['type'] = "MONTHLY"
        frequency['frequency'] = int(freq_value)
        frequency['activateDate'] = build_start_date(start_time)
        frequency['domList'] = build_monthly_domlist(frequency['activateDate'])
    else:
        logger.error("Invalid frequency type, must be minute, hour, day, week or month")
        session.logout()
        sys.exit(2)
        
    return frequency

def build_start_date(start_time):
    try:
        sdt = datetime.datetime.strptime(start_time, '%m/%d/%Y %H:%M')
    except ValueError:
        logger.error("Invalid start time value, please use '%m/%d/%Y %H:%M' format")
        session.logout()
        sys.exit(2)
        
    sdt += datetime.timedelta(minutes=2.5)
    sdt -=  datetime.timedelta(minutes=sdt.minute %5, seconds=sdt.second, microseconds=sdt.microsecond)
    #print sdt
    start_time =int(sdt.strftime("%S"))*1000
    return start_time

def build_weekly_dowlist(adate):
    dowlist = [False,False,False,False,False,False,False,False]
    adatedt = datetime.datetime.utcfromtimestamp(adate/1000)
    if(adatedt.weekday() == 6):
        dowlist[1] = True
    elif(adatedt.weekday() == 0):
        dowlist[2] = True
    elif(adatedt.weekday() == 1):
        dowlist[3] = True
    elif(adatedt.weekday() == 2):
        dowlist[4] = True
    elif(adatedt.weekday() == 3):
        dowlist[5] = True
    elif(adatedt.weekday() == 4):
        dowlist[6] = True
    elif(adatedt.weekday() == 5):
        dowlist[7] = True
    return dowlist

def build_monthly_domlist(adate):
    domlist = [False] * 32
    adatedt = datetime.datetime.utcfromtimestamp(adate/1000)
    domlist[adatedt.day] = True
    return domlist

def create_sla_policy(slainfo):
    try:
        response = client.SppAPI(session, 'ngp/slapolicy').post(data=slainfo)
        logger.info("SLA Policy " + slainfo['name'] + " is created")
        #trying to print response
        
    except client.requests.exceptions.HTTPError as err:
        errmsg = json.loads(err.response.content)
        logger.error(errmsg['response'])

#---------------------------
#script to delete sla policy
#---------------------------

def find_slapolicy(slaname):
    response = client.SppAPI(session, 'ngp/slapolicy').get(path='')
    slalist = response['slapolicies']
    for sla in slalist:
        if(sla['name'].upper() == slaname.upper()):
            return sla
        
    logger.error("Sla policy not found.")
    session.logout()
    sys.exit(3)
        
                
def delete_slapolicy(slapolicy):
    #deleteslapath = slapolicy['name'] + "?action=start&actionname=start"
    try:
        response = client.SppAPI(session, 'ngp/slapolicy/').delete(slapolicy['name'])
        logger.info("Sla policy " + slapolicy['name'] + " is deleted")
    except client.requests.exceptions.HTTPError as err:
        #print err.response.content
        session.logout()
        sys.exit(4)

#--------------------------
# script to edit sla policy
#---------------------------

def edit_sla_policy(editsla_name,editsla_site,editretention_type,editretention_value,editfrequency_type,editfrequency_value,editstart_time):
    #sla_id = find_slapolicy(editsla_name)
    #slainfo = oldslainfo
    slainfo = {"name":editsla_name,
               "version":"1.0",
               "spec":{"simple":True,
                       "subpolicy":[{"type":"REPLICATION",
                                     "software":True,
                                     "retention":{"age":editretention_value},
                                     "trigger":build_frequency(editfrequency_type, editfrequency_value,editstart_time),
                                     "site":editsla_site}]}}
    print slainfo
    return slainfo

def update_sla_policy(slainfo):
    sla_inf = find_slapolicy(slainfo['name'])
    sla_id = sla_inf['id']
    print sla_id
    try:
        response = client.SppAPI(session, 'ngp/slapolicy/').put(path=sla_id, data=slainfo)
        logger.info("SLA Policy " + slainfo['name'] + " is edited")
        #trying to print response
        
    except client.requests.exceptions.HTTPError as err:
        errmsg = json.loads(err.response.content)
        logger.error(errmsg['response'])
        session.logout()
        sys.exit(4)
        
#----------------------------        
# script to assign sla to vms
#----------------------------

def get_vm_info(vms,sla):
    vmarray = []
    for vm in vms:
        vmdata = {}
        searchdata = {"name":vm,"hypervisorType":"vmware"}
        vmsearch = client.SppAPI(session, 'corehv').post(path="/search?resourceType=vm&from=hlo", data=searchdata)['vms']
        if not vmsearch:
            logger.error("Did not find VM " + vm)
            session.logout()
            sys.exit(4)
            break
        for foundvm in vmsearch:
            if(foundvm['name'] == vm):
                vmdata['href'] = foundvm['links']['self']['href']
                vmdata['id'] = foundvm['id']
                vmdata['metadataPath'] = foundvm['metadataPath']
                vmarray.append(copy.deepcopy(vmdata))
                logger.info("Adding VM " + vm + " to SLA " + sla)
                break
          
    return vmarray

#------------------
# Assign sla to vms
#------------------

def get_sla_info(user_sla):
    slaarray = []
    sladata = {}
    slapols = client.SppAPI(session, 'ngp/slapolicy?status=true&subtype=vmware').get()['slapolicies']
    for sla in slapols:
        if(sla['name'] == user_sla):
            sladata['href'] = sla['links']['self']['href']
            sladata['id'] = sla['id']
            sladata['name'] = sla['name']
            slaarray.append(copy.deepcopy(sladata))
            break
    if not slaarray:
        logger.error("No SLA Policy found with given name ")
        session.logout()
        sys.exit(2)
    else:
        return slaarray

def assign_vms_to_sla(vms,sla):
    assigndata = {}
    slainfo = get_sla_info(sla)
    vminfo = get_vm_info(vms,sla)
    assigndata['subtype'] = "vmware"
    assigndata['version'] = "1.0"
    assigndata['resources'] = vminfo
    assigndata['slapolicies'] = slainfo
    resp = client.SppAPI(session, 'spphv').post(path='?action=applySLAPolicies', data=assigndata)
    logger.info("VMs are now assigned")

#--------------------------------------------------------
# Script to restore one or more VMWare VMs by name in SPP
#--------------------------------------------------------

def build_vm_source(restore_vms):
    source = []
    for vm in restore_vms:
        vminfo = get_vm_restore_info(vm)
        if(vminfo is not None):
            source.append(copy.deepcopy(vminfo))
    return source

def get_vm_restore_info(vm):
    vmdata = {}
    searchdata = {"name":vm,"hypervisorType":"vmware"}
    vmsearch = client.SppAPI(session, 'corehv').post(path="/search?resourceType=vm&from=recovery", data=searchdata)['vms']
    if not vmsearch:
        logger.error("Did not find recoverable VM ")
        session.logout()
        sys.exit(2)
    for foundvm in vmsearch:
        if(foundvm['name'] == vm):
            vmdata['href'] = foundvm['links']['self']['href']
            vmdata['metadata'] = {'name':foundvm['name']}
            vmdata['resourceType'] = "vm"
            vmdata['id'] = foundvm['id']
            vmdata['include'] = True
            vmdata['version'] = {}
            vmdata['version']['href'] = foundvm['links']['latestversion']['href']
            vmdata['version']['metadata'] = {'useLatest':True,'name':"Use Latest"}
            #logger.info("Adding VM " + vm + " to restore job")
        
    return vmdata

def build_subpolicy(restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup):
    subpolicy = []
    subpol = {}
    subpol['type'] = "IV"
    subpol['destination'] = {"systemDefined": True}
    subpol['option'] = {}
    subpol['option']['protocolpriority'] = "iSCSI"
    subpol['option']['poweron'] = power_on_after_recovery
    subpol['option']['continueonerror'] = continue_restore_iffails
    subpol['option']['autocleanup'] = overwrite_forcecleanup
    subpol['option']['allowsessoverwrite'] = overwite_virtual_machine
    if not (restore_type.upper()=='production'.upper()):
        subpol['option']['mode'] = restore_type
    else:
        subpol['option']['mode'] = 'recovery'
    subpol['option']['vmscripts'] = False
    subpolicy.append(subpol)
    
    return subpolicy

def restorethe_vms(restore_vms,restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup):
    restore = {}
    sourceinfo = build_vm_source(restore_vms)
    subpolicy = build_subpolicy(restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup)
    restore['subType'] = "vmware"
    restore['spec'] = {}
    restore['spec']['source'] = sourceinfo
    restore['spec']['subpolicy'] = subpolicy
    resp = client.SppAPI(session, 'spphv').post(path='?action=restore', data=restore)
    logger.info("VM is now being restored") 
    
#--------------------------------------
# Script to run a job on-demand for SPP
#--------------------------------------

def find_job_from_name(job_name):
    try:
        response = client.SppAPI(session, 'endeavour').get(path='job')
        joblist = response['jobs']
        for job in joblist:
            if(job['name'].upper() == job_name.upper()):
                return job
        logger.info("No job with provided name found")
        session.logout()
        sys.exit(2)
    except client.requests.exceptions.HTTPError as err:
       # print err.response.content
        session.logout()
        sys.exit(3)

def runthe_job(job):
    jobrunpath = "job/" + job['id'] + "?action=start&actionname=start"
    try:
        response = client.SppAPI(session, 'endeavour').post(path=jobrunpath)
        logger.info("Running job " + job['name'])
    except client.requests.exceptions.HTTPError as err:
        #print err.response.content
        session.logout()
        sys.exit(4)

#-----------------------------------       
# Script to restore the alternate VM
#-----------------------------------

def alternatebuild_subpolicy(restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup,target_name,target_resourceType,target_href,mapvirtualnetwork_source,mapvirtualnetwork_recovery,mapvirtualnetwork_test,mapsubnet,mapRRPdatastore_source,mapRRPdatastore_destination,getall_maps):
    #print getall_maps
    subpolicy = []
    subpol = {}
    subpol['type'] = "IV"
    subpol['destination']= {}
    subpol['destination']['target']={'name':target_name ,'resourceType':target_resourceType , 'href':target_href}
    subpol['mapvirtualnetwork'] ={mapvirtualnetwork_source:{'recovery':mapvirtualnetwork_recovery, 'test':mapvirtualnetwork_test}}
    subpol['mapRRPdatastore']={mapRRPdatastore_source : mapRRPdatastore_destination }
    if(all_maps != ''):
        subpol['mapsubnet'] = getall_maps
    else:
        subpol['mapsubnet'] = {"systemDefined": mapsubnet }
   
    
    subpol['option'] = {}
    subpol['option']['protocolpriority'] = "iSCSI"
    subpol['option']['poweron'] = power_on_after_recovery
    subpol['option']['continueonerror'] = continue_restore_iffails
    subpol['option']['autocleanup'] = overwrite_forcecleanup
    subpol['option']['allowsessoverwrite'] = overwite_virtual_machine
    if not (restore_type.upper()=='production'.upper()):
        subpol['option']['mode'] = restore_type
    else:
        subpol['option']['mode'] = 'recovery'
    subpol['option']['vmscripts'] = False
    subpolicy.append(subpol)
    return subpolicy

def alternate_restorethe_vms(restore_vms,restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup,target_name,target_resourceType,target_href,mapvirtualnetwork_source,mapvirtualnetwork_recovery,mapvirtualnetwork_test,mapsubnet,mapRRPdatastore_source,mapRRPdatastore_destination,all_maps):
    restore = {}
    sourceinfo = build_vm_source(restore_vms)
    subpolicy = alternatebuild_subpolicy(restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup,target_name,target_resourceType,target_href,mapvirtualnetwork_source,mapvirtualnetwork_recovery,mapvirtualnetwork_test,mapsubnet,mapRRPdatastore_source,mapRRPdatastore_destination,all_maps)
    restore['subType'] = "vmware"
    restore['spec'] = {}
    restore['spec']['source'] = sourceinfo
    restore['spec']['subpolicy'] = subpolicy
    resp = client.SppAPI(session, 'spphv').post(path='?action=restore', data=restore)
    #logger.info(resp)
    logger.info("VMs are now being restored") 

def add_mapping():
    
    mapping_type = raw_input("1.DHCP 2.Static\n")
    if (mapping_type=="1"):
        subnet = raw_input("Add subnet or IP address (e.g 192.168.2.15):")
        dhcp = "true"
        dnslist = ''
        metadata = subnet
        dhcp_map = {subnet:{"dhcp":"true" , "dnslist":'', "metadata":{"ip":subnet}}}
        all_maps.append(dhcp_map)
        
    if (mapping_type == "2"):
        subnet = raw_input("Add subnet or IP address (e.g 192.168.2.15):")
        dhcp = "false"
        subnet_mask = raw_input("Enter subnet mask (e.g 255.255.0.0):")
        gateway = raw_input("Enter gateway (e.g 192.168.2.248):")
        dnslist = raw_input("Enter DNS comma seprated:(Leave empty to use source DNS ):")
        static_map = {subnet:{"dhcp":"false", "subnet":subnet, "subnetmask":subnet_mask, "gateway":gateway, "dnslist":dnslist, "metadata":{"ip":subnet}}}
        all_maps.append(static_map)
    
    
    #print(all_maps)
    add_more = raw_input("Add mapping (yes or no):")
    if(add_more == "yes"):
        add_mapping()
    
    return all_maps

#---------------
# Restore Oracle
#---------------
    
def restore_Orc(continue_on_error,database_href,database_version,database_name,database_id,sub_type,autocleanup,allowsessoverwrite,continueonerror,protocol_priority,overwriteExistingDb,maxParallelStreams,initParams):
        restore = {"subType":"oracle",
     "script":{"preGuest":None,"postGuest":None,"continueScriptsOnError":continue_on_error},
     "spec":
     {"source":[{"href":database_href,"resourceType":"database","include":True,
                 "version":{"href":database_version,
                            "metadata":{"useLatest":True}},
                 "metadata":{"name":database_name},"id":database_id}],
      "subpolicy":[{"type":"restore","mode":sub_type,
                    "destination":{"mapdatabase":{database_href:{"name":"","paths":[]}}},
                    "option":{"autocleanup":autocleanup,"allowsessoverwrite":allowsessoverwrite,"continueonerror":continueonerror,"protocolpriority":protocol_priority,
                              "applicationOption":{"overwriteExistingDb":overwriteExistingDb,"maxParallelStreams":maxParallelStreams,"initParams":initParams}},
                    "source":{"copy":{"site":{"href":host + ":443/api/site/1000"}}}}],
      "view":"applicationview"}}
        resp = client.SppAPI(session, 'ngp/application').post(path='?action=restore', data=restore)
        logger.info("Oracle database is restored")
        
def restore_orc_alternate(continue_on_error,database_href,database_version,database_name,database_id,sub_type,traget_href,autocleanup,allowsessoverwrite,continueonerror,protocol_priority):
            restore = {"subType":"",
             "script":{"preGuest":None,"postGuest":None,"continueScriptsOnError":continue_on_error},
             "spec":{"source":[{"href":database_href,"resourceType":"database","include":True,
                                "version":{"href":database_version,
                                           "metadata":{"useLatest":True}},
                                "metadata":{"name":database_name},"id":database_id}],
                     "subpolicy":[{"type":"IA","mode":sub_type,
                                   "destination":
                                   {"target":{"href":traget_href,"resourceType":"applicationinstance"}},
                                   "option":{"autocleanup":autocleanup,"allowsessoverwrite":allowsessoverwrite,"continueonerror":continueonerror,"protocolpriority":protocol_priority},
                                   "source":{"copy":{"site":{"href":host + ":443/api/site/1000"}}}}],
                     "view":"applicationview"}}
            resp = client.SppAPI(session, 'ngp/application').post(path='?action=restore', data=restore)
            logger.info("Oracle database restored with alternate instance")

#-----------   
#Restore SQL
#-----------

def restore_sql(continue_on_error,database_href,database_version,database_name,database_id,sub_type,autocleanup,allowsessoverwrite,continueonerror,overwriteExistingDb,maxParallelStreams,recoverymode):
        restore = {"subType":"sql",
                   "script":{"preGuest":None,"postGuest":None,"continueScriptsOnError":continue_on_error},
                   "spec":{"source":[{"href":database_href,"resourceType":"database","include":True,
                                      "version":{"href":database_version,
                                                 "metadata":{"useLatest":True}},"metadata":{"name":database_name},"id":database_id}],
                           "subpolicy":[{"type":"restore","mode":sub_type,"destination":
                                         {"mapdatabase":{database_href:{"name":"","paths":[]}}},
                                         "option":{"autocleanup":autocleanup,"allowsessoverwrite":allowsessoverwrite,"continueonerror":continueonerror,"applicationOption":{"overwriteExistingDb":overwriteExistingDb,"maxParallelStreams":maxParallelStreams,"recoverymode":recoverymode}},
                                         "source":{"copy":{"site":{"href":host + ":443/api/site/1000"}}}}],
                           "view":"applicationview"}}
        resp = client.SppAPI(session, 'ngp/application').post(path='?action=restore', data=restore)
        logger.info("Sql database is restored")
        
def restore_sql_alternate(continue_on_error,database_href,database_version,database_name,database_id,sub_type,traget_href,autocleanup,allowsessoverwrite,continueonerror,protocol_priority):
            restore = {"subType":"",
             "script":{"preGuest":None,"postGuest":None,"continueScriptsOnError":continue_on_error},
             "spec":{"source":[{"href":database_href,"resourceType":"database","include":True,
                                "version":{"href":database_version,
                                           "metadata":{"useLatest":True}},
                                "metadata":{"name":database_name},"id":database_id}],
                     "subpolicy":[{"type":"IA","mode":sub_type,
                                   "destination":
                                   {"target":{"href":traget_href,"resourceType":"applicationinstance"}},
                                   "option":{"autocleanup":autocleanup,"allowsessoverwrite":allowsessoverwrite,"continueonerror":continueonerror,"protocolpriority":protocol_priority},
                                   "source":{"copy":{"site":{"href":host + ":443/api/site/1000"}}}}],
                     "view":"applicationview"}}
            resp = client.SppAPI(session, 'ngp/application').post(path='?action=restore', data=restore)
            logger.info("Sql database restored with alternate instance")

    ###########   
    # Main menu
    ###########
    
def main_menu():
    #os.system('clear')
    print "Please choose the function number you want to start:\n"
    print "1. Backup Vms"
    print "1. Create Sla Policy"
    print "2. Delete Sla Policy"
    print "3. Edit Sla Policy"
    print "4. Assign Sla Policy to Vms"
    print "5. Restore Vms using Original ESX Host or Cluster"
    print "6. Run Job by name"
    print "7. Restore Vms using Alternate ESX Host or Cluster"
    print "8. Oracle Backup"
    print "9.Oracle Restore"
    print "10.Sql backup"
    print "11.Sql restore"
    print "\n0. Quit\n"
    choice = raw_input(" >>  ")
    exec_menu(choice)
 
    return

#------------- 
# Execute menu
#-------------

def exec_menu(choice):
    #os.system('clear')
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print "Invalid selection, please try again.\n"
            menu_actions['main_menu']()
    return
 
 ##########
 # option 1
 ##########

def create_sla():
    print "Enter Sla details to create sla:\n"
    sla_name = raw_input("Enter Sla name:")
    sla_site = raw_input("Enter Sla site (Primary or Replication):\n")
    #build_site(sla_site)
    retention_type = raw_input("Enter Retention type (Days or Snapshots):\n")
    retention_value = raw_input("Retention value:\n")
    build_retention(retention_type,retention_value)
    frequency_type = raw_input("Enter frequency type (minute, hour, day, week or month):\n")
    frequency_value = raw_input("Enter frequency value:\n")
    start_time = raw_input("Start time, format(mm/dd/yyyy : hh:mm):\n")
    build_frequency(frequency_type,frequency_value,start_time)
    slainfo = build_sla_policy(sla_name,sla_site,retention_type,retention_value,frequency_type,frequency_value,start_time)
    create_sla_policy(slainfo)
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return

##########
# option 2
##########

def delete_sla():
    delete_slaname = raw_input("Enter sla name to delete:\n")
    slapolicy = find_slapolicy(delete_slaname)
    delete_slapolicy(slapolicy)
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return

##########
# option 3
##########

def edit_sla():
    editsla_name = raw_input("Enter Sla name to edit:\n")
    editsla = find_slapolicy(editsla_name)
    editsla_name = editsla['name']
    editsla_site = raw_input("Enter Sla site (Primary or Secondary or DR site):\n")
    #build_site(editsla_site)
    editretention_type = raw_input("Enter Retention type (Days or Snapshots):\n")
    editretention_value = raw_input("Retention value:\n")
    build_retention(editretention_type,editretention_value)
    editfrequency_type = raw_input("Enter frequency type (minute, hour, day, week or month):\n")
    editfrequency_value = raw_input("Enter frequency value:\n")
    editstart_time = raw_input("Start time, format(mm/dd/yyyy : hh:mm):\n")
    build_frequency(editfrequency_type,editfrequency_value,editstart_time)
    slainfo = edit_sla_policy(editsla_name,editsla_site,editretention_type,editretention_value,editfrequency_type,editfrequency_value,editstart_time)
    update_sla_policy(slainfo)
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return

##########
# option 4
##########

def assignsla_tovms():
    
    vcenter_id = raw_input("\nEnter vcenter id for the vms:\n")
    all_vms = client.SppAPI(session, 'api/hypervisor/').get(path= vcenter_id +"/vm?from=hlo")
    instances = all_vms['vms']
    backup_instance = raw_input("\nEnter vms(comma seprated):\n")
    for inst in instances:
        if(inst['name'] == backup_instance):
                backup_href = inst['links']['self']['href']
                backup_id = inst['id']
                backup_metadataPath = inst['metadataPath']
                
    instance_list = backup_instance.split(",")
    instance_info = []
    instdata = {}
    for inst in instance_list:
        for each_inst in instances:
            if(each_inst['name'] == inst):
                instdata['href'] = each_inst['links']['self']['href']
                instdata['id'] = each_inst['id']
                instdata['metadataPath'] = each_inst['metadataPath']
                instance_info.append(copy.deepcopy(instdata))
                logger.info("Adding instance " + inst + " to SLA ")
                break
    all_policies_data = client.SppAPI(session, 'ngp/slapolicy').get(path="?status=true&subtype=vmware")
    all_policies = all_policies_data['slapolicies']
    for pol in all_policies:
        print pol['name']
    assign_sla = raw_input("\nEnter sla's comma seprated from above list to assign to instance:\n")
    sla_list = assign_sla.split(",")
    instance_list = backup_instance.split(",")
    slaarray = []
    sladata = {}
    for sla in sla_list:
        for each_sla in all_policies:
            if(each_sla['name'] == sla):
                sladata['href'] = each_sla['links']['self']['href']
                sladata['id'] = each_sla['id']
                sladata['name'] = each_sla['name']
                slaarray.append(copy.deepcopy(sladata))
                break
        if not slaarray:
            logger.error("No SLA Policy found with name " + user_sla)
            session.logout()
            sys.exit(2)
        
    assigndata = {}
    assigndata['subtype'] = "vmware"
    assigndata['version'] = "1.0"
    assigndata['resources'] = instance_info
    assigndata['slapolicies'] = slaarray
    resp = client.SppAPI(session, 'ngp/hypervisor').post(path='?action=applySLAPolicies', data=assigndata)
    logger.info("Vms are now assigned")
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return
    
##########    
# option 5
##########

def restore_vms():
    restore_vms = raw_input("Enter vm name to restore:\n")
    if(restore_vms is not None):
        restore_vms = restore_vms.split(",")
        build_vm_source(restore_vms)
        #Vm restore destination
        #Restore type(test/production/clone)
        restore_type = raw_input("Restore type (test/production/clone):\n")
        if(restore_type.upper()!="TEST" and  restore_type.upper()!="PRODUCTION" and restore_type.upper()!="CLONE" ):
            logger.error("Restore type must be one of the following: \nTest\nProduction\nClone")
            session.logout()
            sys.exit(2)
        #Advanced options(true/false)
        power_on_after_recovery = raw_input("Power on after recovery (true/false):\n")
        if(power_on_after_recovery.upper()!="TRUE" and power_on_after_recovery.upper()!="FALSE"):
            logger.error("Power on after recovery must be true or false")
            session.logout()
            sys.exit(2)
        overwite_virtual_machine= raw_input("Overwrite Virtual machine (true/false):\n")
        if(overwite_virtual_machine.upper()!="TRUE" and overwite_virtual_machine.upper()!="FALSE"):
            logger.error("Overwrite Virtual machine must be true or false")
            session.logout()
            sys.exit(2)
        continue_restore_iffails= raw_input("Continue with restore even if it fails (true/false):\n")
        if(continue_restore_iffails.upper()!="TRUE" and continue_restore_iffails.upper()!="FALSE"):
            logger.error("Continue with restore even if it fails must be true or false")
            session.logout()
            sys.exit(2)
        overwrite_forcecleanup= raw_input("Allow to overwrite and force clean up of pending old session (true/false):\n")
        if(overwrite_forcecleanup.upper()!="TRUE" and overwrite_forcecleanup.upper()!="FALSE"):
            logger.error("Allow to overwrite and force clean up of pending old session must be true or false")
            session.logout()
            sys.exit(2)
    restorethe_vms(restore_vms,restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup)
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return
    
##########
# option 6
##########

def run_job():
    job_name = raw_input("Enter job name to run:\n")
    job = find_job_from_name(job_name)
    runthe_job(job)
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return

##########
# option 7
##########

def restore_alternate_vms():
    restore_vms = raw_input("Enter vm names to restore(comma seprated):\n")
    if(restore_vms is not None):
        restore_vms = restore_vms.split(",")
        build_vm_source(restore_vms)
    #alternate_options(restore_vms)
        key = raw_input("\nEnter Hypervisor key:\n")
        hypervisor = client.SppAPI(session, 'corehv').get(path="/" + key + "?from=hlo")
        print("vCenter:\n Host Address:"+ hypervisor['hostAddress'])
        view = raw_input("Enter host or cluster:\n")
        if(view == "host"):
            hosts = client.SppAPI(session, 'corehv').get(path="/"+ key +"/host?from=hlo")
            host_list = hosts['hosts']
            print("List of Hosts:\n")
            for h in host_list:
                print "Host:" + h['name']
            vCenter = raw_input("Enter host name you would like to use:\n")
            target_name = vCenter
            target_resourceType = "host"
        elif(view == "cluster"):
            clusters = client.SppAPI(session, 'corehv').get(path="/"+ key +"/cluster?from=hlo")
            cluster_list = clusters['clusters']
            print("List of Clusters:\n")
            for c in cluster_list:
                print "Cluster:" + c['name']
            vCenter = raw_input("Enter cluster name you would like to use:\n")
            target_name = vCenter
            target_resourceType = "cluster"
        else:
            logger.error("Incorrect input!!")
            session.logout()
            sys.exit(2)
            
            
        networks = client.SppAPI(session, 'corehv').get(path="/"+ key +"/network?from=hlo")
        network_list = networks['networks']
        mapvirtualnetwork_recovery = ''
        mapvirtualnetwork_test = ''
        
        #print(network_list)
        #Print "Network Settings:"
        for n in network_list:
            print "Newtork:" + n['nativeObject']['name']
        recovery = raw_input("Production network (enter network from above list):\n")
        test = raw_input("Test network (enter newtwork from above list):\n")
            
        for n in network_list:
            if n['nativeObject']['name'] == recovery:
                 mapvirtualnetwork_recovery = n['links']['self']['href']
            
                
        if(mapvirtualnetwork_recovery == ''):
            logger.error("Incorrect input for Network settings!!")
            session.logout()
            sys.exit(2)
                
        for n in network_list:   
            if n['nativeObject']['name'] == test:
                mapvirtualnetwork_test  = n['links']['self']['href']
            
            
        if(mapvirtualnetwork_test == ''):
            logger.error("Incorrect input for Network settings!!")
            session.logout()
            sys.exit(2)
                
        subnet_option = raw_input("1.Use system defined subnets and IP addresses for VM guest OS on destination \n2.Use original subnets and IP addresses for VM guest OS on destination \n3.Add mappings for subnets and IP addresses for VM guest OS on destination (Enter option number):\n")
        if subnet_option == "1":
            mapsubnet = "true"
            getall_maps = ''
            mapRRPdatastore_source = ''
            mapRRPdatastore_destination = ''
        elif subnet_option == "2":
            mapsubnet = "false"
            getall_maps = ''
            mapRRPdatastore_source = ''
            mapRRPdatastore_destination = ''
        elif subnet_option == "3":
            mapsubnet = ''
            getall_maps = add_mapping()
            #print getall_maps
        else:
            logger.error("Incorrect input for Subnet Options!!")
            session.logout()
            sys.exit(2)
        
            
                    
        volume = client.SppAPI(session, 'corehv').get(path="/"+ key +"/volume?from=hlo")
        print "Destination Datastore:"
        volume_list = volume['volumes']
        mapRRPdatastore_destination = ''
        #print(volume_list)
        for v in volume_list:
            print "Volume:" + v['name']
        destination = raw_input("Destination Datastore (Enter volume from above list):\n")
            
        for v in volume_list:
            if v['name'] == destination:
                mapRRPdatastore_destination = v['links']['self']['href']
            
        if (mapRRPdatastore_destination == ''):
            logger.error("Incorrect input for Destination datastore!!")
            session.logout()
            sys.exit(2)
                
        restore_type = raw_input("Restore type (test/production/clone):\n")
        if(restore_type.upper()!="TEST" and  restore_type.upper()!="PRODUCTION" and restore_type.upper()!="CLONE" ):
            logger.error("Restore type must be one of the following: \nTest\nProduction\nClone")
            session.logout()
            sys.exit(2)
           
        mapsource = client.SppAPI(session, 'corehv').get(path="/"+ key +"/network?from=recovery")
        mapsource_networks =  mapsource['networks']
        for s in mapsource_networks:
            if s['name'] == "172.20.x.x Network":
                mapvirtualnetwork_source = s['links']['latestversion']['href']
                #print mapvirtualnetwork_source
                
        volumesource = client.SppAPI(session, 'corehv').get(path="/"+ key +"/volume?from=recovery")
        volumes_list = volumesource['volumes']
        for v in volumes_list:
            if v['name'] == "BCJ-SPP-BU-VMS":
                mapRRPdatastore_source = v['links']['latestversion']['href']
                #print mapRRPdatastore_source
           
            
        #Advanced options(true/false)
        power_on_after_recovery = raw_input("Power on after recovery (true/false):\n")
        if(power_on_after_recovery.upper()!="TRUE" and power_on_after_recovery.upper()!="FALSE"):
            logger.error("Power on after recovery must be true or false")
            session.logout()
            sys.exit(2)
        overwite_virtual_machine= raw_input("Overwrite Virtual machine (true/false):\n")
        if(overwite_virtual_machine.upper()!="TRUE" and overwite_virtual_machine.upper()!="FALSE"):
            logger.error("Overwrite Virtual machine must be true or false")
            session.logout()
            sys.exit(2)
        continue_restore_iffails= raw_input("Continue with restore even if it fails (true/false):\n")
        if(continue_restore_iffails.upper()!="TRUE" and continue_restore_iffails.upper()!="FALSE"):
            logger.error("Continue with restore even if it fails must be true or false")
            session.logout()
            sys.exit(2)
        overwrite_forcecleanup= raw_input("Allow to overwrite and force clean up of pending old session (true/false):\n")
        if(overwrite_forcecleanup.upper()!="TRUE" and overwrite_forcecleanup.upper()!="FALSE"):
            logger.error("Allow to overwrite and force clean up of pending old session must be true or false")
            session.logout()
            sys.exit(2)
        target_href = network_list[0]['links']['host']['href']
        
    alternate_restorethe_vms(restore_vms,restore_type,power_on_after_recovery,overwite_virtual_machine,continue_restore_iffails,overwrite_forcecleanup,target_name,target_resourceType,target_href,mapvirtualnetwork_source,mapvirtualnetwork_recovery,mapvirtualnetwork_test,mapsubnet,mapRRPdatastore_source,mapRRPdatastore_destination,getall_maps)
          
    del all_maps[:]
            
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return

##########
# option 8
##########

def oracle_backup():
    
    all_databases = client.SppAPI(session, 'api/application/oracle').get(path="/instance?from=hlo")
    instances = all_databases['instances']
    #print instances
    print ("Backup Instances:\n")
    for inst in instances:
        print inst['name']
    backup_instance = raw_input("\nEnter instances(comma seprated) from list:\n")
    for inst in instances:
        if(inst['name'] == backup_instance):
                backup_href = inst['links']['self']['href']
                backup_id = inst['id']
                backup_metadataPath = inst['metadataPath']
                
    oracle_options = raw_input("\n1.Maximum Parallel streams\n2.Assign slaPolicy\n")
    if(oracle_options == '1'):
        max_streams = raw_input("\nEnter maximum parellel streams per database:\n")
        applyOptions = {"resources":[{"href": backup_href,"id":backup_id,"metadataPath":backup_metadataPath}],"options":{"maxParallelStreams":max_streams}}
        backup_opt = client.SppAPI(session, 'ngp/application').post(path='?action=applyOptions', data=applyOptions)
        print ("Options updated for the Oracle instance!")
    elif(oracle_options == '2'):
        all_policies_data = client.SppAPI(session, 'ngp/slapolicy').get(path="?status=true&subtype=oracle")
        all_policies = all_policies_data['slapolicies']
        for pol in all_policies:
                print pol['name']
        assign_sla = raw_input("\nEnter sla's comma seprated from above list to assign to instance:\n")
        instance_list = backup_instance.split(",")
        instance_info = []
        instdata = {}
        for inst in instance_list:
            for each_inst in instances:
                if(each_inst['name'] == inst):
                    instdata['href'] = each_inst['links']['self']['href']
                    instdata['id'] = each_inst['id']
                    instdata['metadataPath'] = each_inst['metadataPath']
                    instance_info.append(copy.deepcopy(instdata))
                    logger.info("Adding instance " + inst + " to SLA ")
                    break

        slaarray = []
        sla_list = assign_sla.split(",")
        sladata = {}
        for sla in sla_list:
            for each_sla in all_policies:
                if(each_sla['name'] == sla):
                    sladata['href'] = each_sla['links']['self']['href']
                    sladata['id'] = each_sla['id']
                    sladata['name'] = each_sla['name']
                    slaarray.append(copy.deepcopy(sladata))
                    break
            if not slaarray:
                logger.error("No SLA Policy found with name " + user_sla)
                session.logout()
                sys.exit(2)
        
        assigndata = {}
        assigndata['subtype'] = "oracle"
        assigndata['version'] = "1.0"
        assigndata['resources'] = instance_info
        assigndata['slapolicies'] = slaarray
        resp = client.SppAPI(session, 'ngp/application').post(path='?action=applySLAPolicies', data=assigndata)
        logger.info("Oracle instances are now assigned")
       
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return

##########
# option 9
##########

def oracle_restore():
    allrecovery_instances = client.SppAPI(session, 'api/application/oracle').get(path="/instance?from=recovery")
    recovery_instances = allrecovery_instances['instances']
    print("\nInstances:\n")
    for inst in recovery_instances:
        print inst['name']
    instance_torecover = raw_input("\nSelect instance to restore from above list:")
    for inst in recovery_instances:
        if inst['name'] == instance_torecover:
            inst_id = inst['id']
            break
    all_databases = client.SppAPI(session, 'api/application/oracle').get(path="/instance/" +inst_id+ "/database?from=recovery")
    databases = all_databases['databases']
    for data in databases:
        print data['name']
    database_torestore = raw_input("\nEnter database name to restore:\n")
    for data in databases:
        if(data['name'] == database_torestore):
            database_id = data['id']
            database_href = data['links']['self']['href']
            database_version = data['links']['latestversion']['href']
            database_resourcetype = data['resourceType']
            database_name = data['name']
            break
       
    continue_on_error = raw_input("Continue script on error(true/false):")
    autocleanup = raw_input("\nRollback all the changes on failure(true/false):\n")
    allowsessoverwrite = raw_input("\nAllow session overwrite(true/false):\n")
    continueonerror = raw_input("\nContinue with restore even if it fails(true/false):\n")
    overwriteExistingDb = raw_input("\nOverwrite existing database(true/false):\n")
    maxParallelStreams = raw_input("\nMaximum Parallel Streams per Database:\n")
    
    sub_type = raw_input("\nEnter subpolicy type:(Test,Production,Instant_Access):")
    if sub_type.upper() == "TEST":
        sub_type = "test"
        protocol_priority = None
        initParams = raw_input("\nEnter Initial parameters:\n")
    elif sub_type.upper() == "PRODUCTION":
        sub_type = "production"
        protocol_priority = None
        initParams = raw_input("\nEnter Initial parameters:\n")
    elif sub_type.upper() == "INSTANT_ACCESS":
        sub_type = "test"
        protocol_priority = raw_input("\nEnter protocol priority:\n")
        initParams = None   
    
    destination = raw_input("\n1.Restore to original instance\n2.Restore to alternate instance")
    if destination == "1":
        restore_Orc(continue_on_error,database_href,database_version,database_name,database_id,sub_type,autocleanup,allowsessoverwrite,continueonerror,overwriteExistingDb,maxParallelStreams,protocol_priority,initParams)
    elif destination == '2':
        print "\nInstances:"
        all_databases = client.SppAPI(session, 'api/application/oracle').get(path="/instance?from=hlo")
        instances = all_databases['instances']
        for inst in instances:
            print inst['name']
        target_instance = raw_input("\nEnter target instance from instance list:")
        for inst in instances:
            if inst['name'] == target_instance:
                traget_href = inst['links']['self']['href']
        restore_orc_alternate(continue_on_error,database_href,database_version,database_name,database_id,sub_type,traget_href,autocleanup,allowsessoverwrite,continueonerror,protocol_priority)
        
        
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return

##########
#option 10
##########

def sql_backup():
    all_databases = client.SppAPI(session, 'api/application/sql').get(path="/instance?from=hlo")
    instances = all_databases['instances']
    #print instances
    print ("Backup Instances:\n")
    for inst in instances:
        print inst['name']
    backup_instance = raw_input("\nEnter instances(comma seprated) from list:\n")
    for inst in instances:
        if(inst['name'] == backup_instance):
                backup_href = inst['links']['self']['href']
                backup_id = inst['id']
                backup_metadataPath = inst['metadataPath']
                
    sql_options = raw_input("\n1.Maximum Parallel streams\n2.Assign slaPolicy\n")
    if(sql_options == '1'):
        max_streams = raw_input("\nEnter maximum parellel streams per database:\n")
        log_backup = raw_input("\nEnable log backup(true/false):\n")
        if(log_backup == "false"):
            applyOptions = {"resources":[{"href": backup_href,"id":backup_id,"metadataPath":backup_metadataPath}],"options":{"maxParallelStreams":max_streams,"logbackup":{}}}
        else: 
            frequency = raw_input("\nFrequency:\n")
            frequency_type = raw_input("\nFrequency type(Minute/Hour,Day,Week,Month):\n")
            trigger_time = raw_input("\nEnter trigger time(HH:MM:SS AM)")
            
            applyOptions = {"resources":[{"href": backup_href,"id":backup_id,"metadataPath":backup_metadataPath}],"options":{"maxParallelStreams":max_streams,"logbackup":{"performlogbackup":True,"rpo":{"frequency":frequency,"frequencyType":frequency_type.upper(),"triggerTime":trigger_time,"metadata":{"activateDate":1521518400000}}}}}
        
        backup_opt = client.SppAPI(session, 'ngp/application').post(path='?action=applyOptions', data=applyOptions)
        print ("Options updated for the sql instance!")
        
    elif(sql_options == '2'):
        all_policies_data = client.SppAPI(session, 'ngp/slapolicy').get(path="?status=true&subtype=oracle")
        all_policies = all_policies_data['slapolicies']
        for pol in all_policies:
                print pol['name']
        assign_sla = raw_input("\nEnter sla's comma seprated from above list to assign to instance:\n")
        instance_list = backup_instance.split(",")
        instance_info = []
        instdata = {}
        for inst in instance_list:
            for each_inst in instances:
                if(each_inst['name'] == inst):
                    instdata['href'] = each_inst['links']['self']['href']
                    instdata['id'] = each_inst['id']
                    instdata['metadataPath'] = each_inst['metadataPath']
                    instance_info.append(copy.deepcopy(instdata))
                    logger.info("Adding instance " + inst + " to SLA ")
                    break

        slaarray = []
        sla_list = assign_sla.split(",")
        sladata = {}
        for sla in sla_list:
            for each_sla in all_policies:
                if(each_sla['name'] == sla):
                    sladata['href'] = each_sla['links']['self']['href']
                    sladata['id'] = each_sla['id']
                    sladata['name'] = each_sla['name']
                    slaarray.append(copy.deepcopy(sladata))
                    break
            if not slaarray:
                logger.error("No SLA Policy found with name " + user_sla)
                session.logout()
                sys.exit(2)
        
        assigndata = {}
        assigndata['subtype'] = "sql"
        assigndata['version'] = "1.0"
        assigndata['resources'] = instance_info
        assigndata['slapolicies'] = slaarray
        resp = client.SppAPI(session, 'ngp/application').post(path='?action=applySLAPolicies', data=assigndata)
        logger.info("sql instnaces are now assigned")
       
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return

##########
#option 11
##########

def sql_restore():
    allrecovery_instances = client.SppAPI(session, 'api/application/sql').get(path="/instance?from=recovery")
    recovery_instances = allrecovery_instances['instances']
    print("\nInstances:\n")
    for inst in recovery_instances:
        print inst['name']
    instance_torecover = raw_input("\nSelect instance to restore from above list:")
    for inst in recovery_instances:
        if inst['name'] == instance_torecover:
            inst_id = inst['id']
            break
    all_databases = client.SppAPI(session, 'api/application/oracle').get(path="/instance/" +inst_id+ "/database?from=recovery")
    databases = all_databases['databases']
    for data in databases:
        print data['name']
    database_torestore = raw_input("\nEnter database name to restore:\n")
    for data in databases:
        if(data['name'] == database_torestore):
            database_id = data['id']
            database_href = data['links']['self']['href']
            database_version = data['links']['latestversion']['href']
            database_resourcetype = data['resourceType']
            database_name = data['name']
            break
       
    continue_on_error = raw_input("Continue script on error(true/false):")
    autocleanup = raw_input("\nRollback all the changes on failure(true/false):\n")
    allowsessoverwrite = raw_input("\nAllow session overwrite(true/false):\n")
    continueonerror = raw_input("\nContinue with restore even if it fails(true/false):\n")
    overwriteExistingDb = raw_input("\nOverwrite existing database(true/false):\n")
    maxParallelStreams = raw_input("\nMaximum Parallel Streams per Database:\n")
    
    sub_type = raw_input("\nEnter subpolicy type:(Test,Production,Instant_Access):")
    if sub_type.upper() == "TEST":
        sub_type = "test"
        protocol_priority = None
        recoverymode = raw_input("\nEnter recovery mode(recovery/norecovery):\n")
    elif sub_type.upper() == "PRODUCTION":
        sub_type = "production"
        protocol_priority = None
        recoverymode = raw_input("\nEnter recovery mode(recovery/norecovery):\n")
    elif sub_type.upper() == "INSTANT_ACCESS":
        sub_type = "test"
        protocol_priority = raw_input("\nEnter protocol priority:\n")
        recoverymode = None
    destination = raw_input("\n1.Restore to original instance\n2.Restore to alternate instance")
    if destination == "1":
        restore_sql(continue_on_error,database_href,database_version,database_name,database_id,sub_type,autocleanup,allowsessoverwrite,continueonerror,overwriteExistingDb,maxParallelStreams,recoverymode)
    elif destination == '2':
        print "\nInstances:"
        all_databases = client.SppAPI(session, 'api/application/oracle').get(path="/instance?from=hlo")
        instances = all_databases['instances']
        for inst in instances:
            print inst['name']
        target_instance = raw_input("\nEnter target instance from instance list:")
        for inst in instances:
            if inst['name'] == target_instance:
                traget_href = inst['links']['self']['href']
        if(target_href is None):
            logger.error("\nTarget Instnace not found ")
            session.logout()
            sys.exit(2)
            
        restore_sql_alternate(continue_on_error,database_href,database_version,database_name,database_id,sub_type,traget_href,autocleanup,allowsessoverwrite,continueonerror,protocol_priority)
    print "12. Back"
    print "0. Quit" 
    choice = raw_input(" >>  ")
    exec_menu(choice)
    return


        
 #------------------    
 # Back to main menu
 #------------------
def back():
    menu_actions['main_menu']()
 #-------------
 # Exit program
 #-------------
def exit():
    sys.exit()
    
########################
#    MENUS DEFINITIONS
########################
 
menu_actions = {
    'main_menu': main_menu,
    '1': create_sla,
    '2': delete_sla,
    '3': edit_sla,
    '4': assignsla_tovms,
    '5': restore_vms,
    '6': run_job,
    '7': restore_alternate_vms,
    '8': oracle_backup,
    '9': oracle_restore,
    '10': sql_backup,
    '11': sql_restore,
    '12': back,
    '0': exit,
}
 
#########################
#      MAIN PROGRAM
#########################
 
if __name__ == "__main__":
    print "Welcome\n"  
    parser = SafeConfigParser()
    parser.read('configSettings.ini')
    username = parser.get('credentials', 'username')
    password = parser.get('credentials', 'password')
    password = base64.b64encode(password)
    host = parser.get('credentials', 'host')
    #validate_input(username,password,host) 
    try:
        session = client.SppSession(host,username,password)
        session.login()
        print "User logged in!!!\n"
         # Launch main menu
        #main_menu()
    except:
        logger.error("Login failed!!!\nInvalid Credentials")
        
     # Launch main menu
    main_menu()
   