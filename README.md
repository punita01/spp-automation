# README #

This README documents whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Python script to automate the SPP client
* SDK: Using text file to take user input parameters and performing the SPP functionalities
* CLI: The user is prompted through command line menu driven options to provide input and perform functions

### How do I get set up? ###

* Python 2.7
* SDK:
	- Give parameter values in 'userinput.txt' file in SppClient->sdk folder
	- Run the python script in 'SPP_automation.py' file
	- Select the 'userinput' file when the file browser prompts

* CLI:
	- Run the scrip in 'SPP_CLI.py' file
	- A menu with all the functionalities will appear on the command line
	- Select the menu option and follow the promots to complete the functionality


### Who do I talk to? ###

* Punita Repe
* email : prepe@catalogicsoftware.com
